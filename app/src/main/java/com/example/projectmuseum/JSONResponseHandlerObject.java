package com.example.projectmuseum;

import android.os.Build;
import android.util.JsonReader;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.ToLongFunction;

public class JSONResponseHandlerObject {

    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    private Object object;


    public JSONResponseHandlerObject(Object obj) {
            this.object = obj;
        }

    /**
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream() throws IOException {
        readJsonStream();
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readObjects(reader);
        } finally {
            reader.close();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readObjects(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("timeFrame")) {
                readArrayTime(reader);
            }
            else if (name.equals("categories")) {
                readArrayCategories(reader);
            }
            else if (name.equals("technicalDetails")) {
                readArrayDetails(reader);
            }
            else if (name.equals("pictures")) {
                readArrayPictures(reader);
            }
            else {
                readArrayObject(reader);
            }
        }
        reader.endObject();
    }

    private void readArrayObject(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext() ) {
            String name = reader.nextName();
            if(name.equals("name")){
                object.setName(reader.nextString());
            }
            else if(name.equals("description")){
                object.setDescription(reader.nextString());
            }
            else if(name.equals("year")) {
                object.setYear(reader.nextInt());
            }
            else if(name.equals("brand")) {
                object.setBrand(reader.nextString());
            }
            else if(name.equals("working")) {
                if(reader.nextBoolean() == true)  {
                    object.setWorking("oui");
                }
                else {
                    object.setWorking("non");
                }
            }
            else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void readArrayTime(JsonReader reader) throws IOException {
        reader.beginObject();
        List<String> t = new ArrayList<String>();
        while(reader.hasNext()){
            String str = Integer.toString(reader.nextInt());
            t.add(str);
        }
        String[] timeFrame = new String[t.size()];
        t.toArray(timeFrame);
        object.setIdTimeFrame(timeFrame);
        reader.endObject();
    }

    private void readArrayCategories(JsonReader reader) throws IOException {
        reader.beginObject();
        List<String> t = new ArrayList<String>();
        while(reader.hasNext()){
            t.add(reader.nextString());
        }
        String[] cat = new String[t.size()];
        t.toArray(cat);
        object.setIdCategories(cat);
        reader.endObject();
    }

    private void readArrayDetails(JsonReader reader) throws IOException {
        reader.beginObject();
        List<String> t = new ArrayList<String>();
        while(reader.hasNext()){
            t.add(reader.nextString());
        }
        String[] details = new String[t.size()];
        t.toArray(details);
        object.setIdTechnicalDetails(details);
        reader.endObject();
    }

    private void readArrayPictures(JsonReader reader) throws IOException {
        reader.beginObject();
        List<String> t = new ArrayList<String>();
        while(reader.hasNext()){
            t.add(reader.nextString());
        }
        String[] pic = new String[t.size()];
        t.toArray(pic);
        object.setIdPictures(pic);
        reader.endObject();
    }
}
