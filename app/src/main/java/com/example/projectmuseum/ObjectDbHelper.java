package com.example.projectmuseum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.Arrays;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class ObjectDbHelper extends SQLiteOpenHelper {
    private static final String TAG = ObjectDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "object.db";

    public static final String TABLE_NAME = "object";

    public static final String _ID = "_id";
    public static final String COLUMN_OBJECT_ID = "idObject";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME_FRAME = "timeFrame";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_TECHNICAL_DETAILS = "technicalDetails";
    public static final String COLUMN_WORKING = "working";
    public static final String COLUMN_PICTURES = "pictures";

    public ObjectDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME + " TEXT NOT NULL, " +
                COLUMN_OBJECT_ID + " INTEGER, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_YEAR + " TEXT, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_TECHNICAL_DETAILS + " TEXT, " +
                COLUMN_WORKING + " TEXT, " +
                COLUMN_PICTURES + " TEXT " + ")";

        db.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean addObject(Object object){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, object.getName());
        cv.put(COLUMN_OBJECT_ID, object.getId());
        cv.put(COLUMN_CATEGORIES, String.join(" ", object.getCategories()));
        cv.put(COLUMN_DESCRIPTION, object.getDescription());
        cv.put(COLUMN_TIME_FRAME, String.join(" ", object.getTimeFrame()));
        cv.put(COLUMN_YEAR, object.getYear());
        cv.put(COLUMN_BRAND, object.getBrand());
        cv.put(COLUMN_TECHNICAL_DETAILS, String.join(" ", object.getTechnicalDetails()));
        cv.put(COLUMN_WORKING, object.getWorking());
        cv.put(COLUMN_PICTURES, String.join(" ", object.getPictures()));

        long rowID = 0;
        db.insert(TABLE_NAME, null, cv);
        db.close();

        return (rowID != -1);
    }

    public static Object cursorToObject(Cursor cursor){
        Object obj;
        if(cursor != null){
            obj = new Object(
                    cursor.getLong(cursor.getColumnIndex(_ID)),
                    cursor.getLong(cursor.getColumnIndex(COLUMN_OBJECT_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES)).split(" "),
                    cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME)).split(" "),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_YEAR)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICAL_DETAILS)).split(" "),
                    cursor.getString(cursor.getColumnIndex(COLUMN_WORKING)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PICTURES)).split(" ")
            );
            return obj;
        }
        else {
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void populate() {
        addObject(new Object("bandes magnétiques"));
        addObject(new Object("ScanMan 32"));
        addObject(new Object("Câbles et terminateurs SCSI"));
        addObject(new Object("Apple //c"));
        addObject(new Object("Toughbook"));
        addObject(new Object("Téléphone GSM"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public Cursor fetchAllObjects() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        // call db.query()
        cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
}
