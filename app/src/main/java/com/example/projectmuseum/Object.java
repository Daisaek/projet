package com.example.projectmuseum;

import android.os.Parcel;
import android.os.Parcelable;


public class Object implements Parcelable {

    public static final String TAG = Object.class.getSimpleName();

    private long id;

    private long idObject;
    private String name;
    private String[] categories;
    private String description;
    private String[] timeFrame;
    private int year;
    private String brand;
    private String[] technicalDetails;
    private String working; //"oui" ou "non"
    private String[] pictures;

    public  Object(){}

    public Object(String name){
        this.name = name;
    }

    public Object(long id, long object, String name, String[] cate, String descrip, String[] time, int year, String brand,
                  String[] tech, String work, String[] pic){
        this.id = id;
        this.idObject = object;
        this.name = name;
        this.categories = cate;
        this.description = descrip;
        this.timeFrame = time;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = tech;
        this.working = work;
        this.pictures = pic;
    }

    public long getId(){ return id; }

    public long getIdObject(){ return idObject; }

    public String getName(){ return name; }

    public String[] getCategories(){ return categories; }

    public String getDescription(){ return description; }

    public String[] getTimeFrame(){ return timeFrame; }

    public int getYear(){ return year; }

    public String getBrand(){ return brand; }

    public String[] getTechnicalDetails(){ return technicalDetails; }

    public String getWorking(){ return working; }

    public String[] getPictures(){ return pictures; }

    public void setId(long id){ this.id = id; }

    public void setIdObject(long id){ this.idObject = id; }

    public void setName(String name){ this.name = name; }

    public void setIdCategories(String[] cat){ this.categories = cat; }

    public void setDescription(String desc){ this.description = desc; }

    public void setIdTimeFrame(String[] time){ this.timeFrame = time; }

    public void setYear(int year){ this.year = year; }

    public void setBrand(String brand){ this.brand = brand; }

    public void setIdTechnicalDetails(String[] tech){ this.technicalDetails = tech; }

    public void setWorking(String work){ this.working = work; }

    public void setIdPictures(String[] pic){ this.pictures = pic; }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Object> CREATOR = new Creator<Object>()
    {
        @Override
        public Object createFromParcel(Parcel source)
        {
            return new Object(source);
        }

        @Override
        public Object[] newArray(int size)
        {
            return new Object[size];
        }
    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeStringArray(categories);
        dest.writeString(description);
        dest.writeStringArray(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeStringArray(technicalDetails);
        dest.writeString(working);
        dest.writeStringArray(pictures);
    }

    public Object(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.categories = in.createStringArray();
        this.description = in.readString();
        this.timeFrame = in.createStringArray();
        this.year = in.readInt();
        this.brand = in.readString();
        this.technicalDetails = in.createStringArray();
        this.working = in.readString();
        this.pictures = in.createStringArray();
    }
}
