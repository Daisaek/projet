package com.example.projectmuseum;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final String[] items = new String[] { "PDA iPAQ 3630", "MTM", "Jauge de contrôle pour cartes perforées"};;
    private static final String[] urls = new String[] {"https://demo-lia.univ-avignon.fr/cerimuseum/items/w1s/thumbnail",
            "https://demo-lia.univ-avignon.fr/cerimuseum/items/xnw/thumbnail","https://demo-lia.univ-avignon.fr/cerimuseum/items/w1s/thumbnail4zh"}  ;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }
        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            holder.bindModel(items[position], urls[position]);
        }
        @Override
        public int getItemCount() {
            return(items.length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView label = null;
        ImageView icon = null;
        private View context;

        RowHolder(View row) {
            super(row);

            label = (TextView) row.findViewById(R.id.label);
            icon = (ImageView)row.findViewById(R.id.icon);
        }
        @Override
        public void onClick(View v){
            // Do something in response to the  click
            final String item = label.getText().toString();
            //Intent
            Intent intent = new Intent(MainActivity.this, ObjectActivity.class);
            //intent.putExtra();
            startActivity(intent);

        }
        void bindModel(String item, String url) {
            label.setText(item);
            //Glide.with(this.context).load(url).into(icon);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


